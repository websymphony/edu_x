json.extract! teacher, :name
json.report teacher.progresses.group_by(&:student).each do |student, progresses|
  json.student do
    json.name student.name
    json.url student_url(student, format: :json)
    json.progress do
      json.array! progresses.each do |progress|
        json.partial! 'progresses/progress', progress: progress
      end
    end
  end
end
