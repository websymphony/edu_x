json.extract! student, :name
json.progress student.progresses do |progress|
  json.partial! 'progresses/progress', progress: progress
end
