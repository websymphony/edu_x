json.lesson_part progress.part.to_s
json.lesson_name progress.part_lesson.name
json.part_name progress.part_name
json.course_name progress.course_name
json.completed_at progress.created_at.to_s
