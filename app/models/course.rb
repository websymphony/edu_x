# frozen_string_literal: true

class Course < ApplicationRecord
  belongs_to :teacher
  has_many :course_lessons
  has_many :lessons, through: :course_lessons
  has_many :progresses, through: :course_lessons

  validates :name, presence: true

  def to_s
    name
  end
end
