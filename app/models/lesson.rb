# frozen_string_literal: true

class Lesson < ApplicationRecord
  has_many :parts
  has_many :course_lessons

  validates :name, presence: true

  def to_s
    name
  end
end
