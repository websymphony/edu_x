# frozen_string_literal: true

class Progress < ApplicationRecord
  belongs_to :student
  belongs_to :part
  belongs_to :course_lesson, optional: true

  delegate :previous, :lesson, :name, to: :part, prefix: true
  delegate :lesson, to: :course_lesson, prefix: true
  delegate :course, to: :course_lesson, allow_nil: true

  validates :student_id, presence: true
  validates :part_id, presence: true
  validate :lessons_can_only_be_completed_in_proper_sequence
  validate :lessons_can_only_be_taken_with_proper_course

  def course_name
    course&.name || 'Not taken with any course'
  end

  private

  def lessons_can_only_be_completed_in_proper_sequence
    return if part.blank? || part_previous.blank?
    if Progress.find_by(part: part_previous, student: student).blank?
      errors.add(:part_id, "Please complete '#{part_previous.detailed_to_s}' before trying to complete '#{part.detailed_to_s}'")
    end
  end

  def lessons_can_only_be_taken_with_proper_course
    return if part.blank? || course_lesson.blank?
    if !part_lesson.eql?(course_lesson_lesson)
      errors.add(:course_lesson_id, "'#{part.detailed_to_s}' is not offered with '#{course_lesson}'. Please select a course that offers 'Lesson: #{part_lesson.number} - #{part_lesson}'")
    end
  end
end
