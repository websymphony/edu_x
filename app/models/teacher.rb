# frozen_string_literal: true

class Teacher < ApplicationRecord
  has_many :courses
  has_many :course_lessons, through: :courses
  has_many :progresses, through: :course_lessons
  has_many :students, through: :progresses

  validates :name, presence: true
end
