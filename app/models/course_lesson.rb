# frozen_string_literal: true

class CourseLesson < ApplicationRecord
  belongs_to :course
  belongs_to :lesson
  has_many :progresses
  has_many :parts, through: :lesson

  delegate :number, to: :lesson, prefix: true

  validates :course, presence: true
  validates :lesson, presence: true
  validates_uniqueness_of :lesson, scope: :course

  scope :available_by_parts, lambda { |parts|
    CourseLesson.includes(:course, :progresses, lesson: :parts)
                .where(parts: { id: parts })
                .order('lessons.number asc')
  }

  def to_s
    "Course: #{course} | Lesson: #{lesson_number} - #{lesson}"
  end
end
