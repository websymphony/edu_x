# frozen_string_literal: true

class Student < ApplicationRecord
  has_many :progresses

  validates :name, presence: true
end
