# frozen_string_literal: true

class Part < ApplicationRecord
  belongs_to :lesson
  belongs_to :previous, class_name: 'Part', optional: true
  has_many :progresses

  delegate :number, to: :lesson, prefix: true

  validates :lesson, presence: true
  validates :number, presence: true
  validates :name, presence: true
  validates :previous, uniqueness: true

  scope :by_numbers, lambda { |lesson_number, part_number|
    joins(:lesson).find_by(lessons: { number: lesson_number },
                           number: part_number)
  }
  scope :ordered_parts_with_lesson, lambda {
    Part.includes(:lesson).order(Arel.sql('lessons.number asc, parts.number asc'))
  }
  scope :available_for_student_to_take, lambda { |student|
    parts_taken = student.progresses.pluck(:part_id)
    next_part_to_take = ordered_parts_with_lesson.where.not(id: parts_taken)
                                                 .first&.id
    available_parts = (parts_taken << next_part_to_take).compact
    ordered_parts_with_lesson.where(id: available_parts)
  }

  def to_s
    "Lesson: #{lesson_number} Part: #{number}"
  end

  def detailed_to_s
    "#{self} | Lesson: #{lesson} | Part: #{name}"
  end
end
