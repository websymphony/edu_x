# frozen_string_literal: true

class ProgressesController < ApplicationController
  before_action :set_lookup_data

  def new
    @progress = Progress.new
  end

  def create
    if (_progress_params = params[:progress])
      _progress_params[:part_id] ||= Part.by_numbers(_progress_params[:lesson_number],
                                                     _progress_params[:part_number]).try(:id)
    end
    @progress = @student.progresses.new(progress_params)

    respond_to do |format|
      if @progress.save
        format.html { redirect_to student_url(@progress.student), notice: 'Progress was successfully saved.' }
        format.json do
          render partial: 'progresses/progress', locals: { progress: @progress },
                 status: :created
        end
      else
        format.html { render :new }
        format.json { render json: { errors: [{ progress: @progress.errors }] }, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_lookup_data
    @student = Student.find(params[:student_id])
    @parts = Part.available_for_student_to_take(@student)
    @course_lessons = CourseLesson.available_by_parts(@parts)
  end

  def progress_params
    params.require(:progress).permit(:part_id, :course_lesson_id, :progress_id)
  end
end
