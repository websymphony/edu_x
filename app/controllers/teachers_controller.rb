# frozen_string_literal: true

class TeachersController < ApplicationController
  def index
    @teachers = Teacher.all
  end

  def show
    @teacher = Teacher.eager_load(progresses: [:student, part: [:lesson], course_lesson: [:course]],
                                  courses: [:progresses])
                      .order('progresses.student_id ASC, lessons.number desc, parts.number desc')
                      .find(params[:id])
  end
end
