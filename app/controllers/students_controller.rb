# frozen_string_literal: true

class StudentsController < ApplicationController
  def index
    @students = Student.all
  end

  def show
    @student = Student.includes(progresses: { part: [:lesson], course_lesson: [:course] })
                      .order('lessons.number desc, parts.number desc, progresses.id desc')
                      .find(params[:id])
  end

  def new
    @student = Student.new
  end

  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json do
          render partial: 'students/student', locals: { student: @student },
                 status: :created,
                 location: @student
        end
      else
        format.html { render :new }
        format.json { render json: { errors: [{ student: @student.errors }] }, status: :unprocessable_entity }
      end
    end
  end

  private

  def student_params
    params.require(:student).permit(:name)
  end
end
