# README

## Description:

This application is for students to complete lessons and record their progress . Each lesson has 3 parts - 1, 2 and 3. There are 100 lessons in total.

This application allows for all functions to be completed either via browser or via an API.

Demo URL: https://edu-x.herokuapp.com/

Instructions to test the API are further below.

---

## Requirements:

- Ruby Version: 2.5.1
- bundler
- Postgresql

---

## Instructions for setting up the app

1.  `git clone git@bitbucket.org:websymphony/edu_x.git`
2.  `cd edu_x`
3.  `bundle install`
4.  `rake db:setup` This adds fake seed data with 100 lessons each with 3 parts, along with some courses, students, teachers etc. to play around.
5.  `rails t` To run tests
6.  `rails s` To run the server

---

## Solution Overview:

### Models:

- **Teacher** : Self explanatory
- **Student** : Self explanatory
- **Lesson** : Lesson and parts are kept as separate concepts. With a lesson having many parts. Although, to keep things simple, there could have been only a single lesson model with `part_number` as an attribute. But in real world scenario, `part` of a lesson can have additional information like description, instructions etc. that we won't want to capture on the lesson itself.
- **Part** : Belongs to Lesson. Has a `name` attribute along with a `number` to identify it.
- **Course** : Belongs to a teacher. Concept of `Class` is expressed as `Course` since `Class` is a reserved keyword in rails.
- **CourseLesson** : Association table to keep note of lessons that are offered as part of a course.
- **Progress** : Belongs to a `Student`, `Part` and `CourseLesson`. Allows to have **null** for `course_lesson_id` when lesson is taken standalone and not with a course.

### Notes:
* As part of the requirement 3 of the assignment, lessons can only be finished in an increasing order. Although, there is a validation to make sure that progress happens linearly. The UI also filters the available dropdowns to select the course/part depending upon which lesson/parts have been previously completed.

### Assumptions:

- Progress as shown in the detail view for a student or as part of the teacher's report of a student, is considered to be a **sorted collection** of all the lesson/parts completed by the student. Sorting is done on the basis of **lesson number and part number**, where the largest numbers are first. For e.g. If a student has completed 4 lessons/parts, L1P1, L1P2, L1P3 and L2P1. Student's complete progress will be [L2P1, L1P3, L1P2, L1P1] and in that order. First item signifying the farthest the student has gone in lessons.
- Single course can only be taught by one teacher. If a course can be taught by multiple teachers, course model can become a lookup model. And there will be need for an additional association table `course_teachers` to capture many_to_many relationship.
- In current design a teacher can get to students through via: **teacher -> course -> course_lesson -> progress -> student**. If there is a requirement of restricting which lessons a student can see depending upon which courses they have been enrolled in. Then an association table `enrollments` with `course_id` and `student_id` can be used.

### Caveats:

- **`Part`** model has `previous_id` to figure out the reverse ordering of lessons and their parts. This can also be accomplished via introspecting lesson numbers and part numbers, but will require sql calls. We are trading sql calls during reads for storage of `previous_id` in current design.
- In current implementation, API is implemented in the same controllers with html request handling. In a production system, APIs should be handled through dedicated versioned endpoints for keeping the responsibilities of API and html content separate.
- In current solution there is no authentication/authorization implementation. This allows anyone to view any student's profile, update their progress and also view any teacher's reports. In a production system, access will be scoped by the logged in user and their role (teacher/student) in the system.
- Currently all the listings pages are showing all entities like Teachers, Students, Progress at once. In a production app, listings will need to be paginated so as to not query all data at once.
- Currently there is no restriction on the number of times a student
  can take a particular lesson. If that is a requirement, additional validation
  will need to be added to the **Progress** model.
- Although, current tests have 100% coverage. In a production system, I'll also add system tests to drive the application through a browser.

---

## Deployed Application:

The solution has been deployed at heroku for ease of testing:
https://edu-x.herokuapp.com/

### Supported functionality via Browser and as well as API:

**For students:**

1.  Listing students.
2.  Creating a new student.
3.  Viewing student details, including progress of all the lessons they have completed.
4.  Creating progress for a student linearly where a lesson can only be taken if all previous lessons have been taken.

**For teachers:**

5.  Listing teachers.
6.  Viewing teacher details, including report of progress of students, on the courses offered by the teacher.

#### Examples for manual testing of API:

#### 1. Listing students:

URL: https://edu-x.herokuapp.com/students.json

EXAMPLE REQUEST:

```
curl -X GET \
  https://edu-x.herokuapp.com/students.json
```

HTTP RESPONSE CODE: **200**

RESPONSE:

```json
[
  {
    "name": "Jed Kessler",
    "url": "https://edu-x.herokuapp.com/students/1.json"
  },
  {
    "name": "Miss Jacquie Schumm",
    "url": "https://edu-x.herokuapp.com/students/2.json"
  },
  {
    "name": "Lisa Medhurst",
    "url": "https://edu-x.herokuapp.com/students/3.json"
  },
  {
    "name": "Isaac O'Connell",
    "url": "https://edu-x.herokuapp.com/students/4.json"
  },
  {
    "name": "Roderick Labadie DVM",
    "url": "https://edu-x.herokuapp.com/students/5.json"
  },
  {
    "name": "Marget Connelly",
    "url": "https://edu-x.herokuapp.com/students/6.json"
  },
  {
    "name": "Tawna Gulgowski",
    "url": "https://edu-x.herokuapp.com/students/7.json"
  },
  {
    "name": "Maureen Hintz",
    "url": "https://edu-x.herokuapp.com/students/8.json"
  },
  {
    "name": "Analisa Schulist",
    "url": "https://edu-x.herokuapp.com/students/9.json"
  },
  {
    "name": "Colleen Kling",
    "url": "https://edu-x.herokuapp.com/students/10.json"
  },
  { "name": "rita", "url": "https://edu-x.herokuapp.com/students/11.json" },
  { "name": "Test", "url": "https://edu-x.herokuapp.com/students/12.json" }
]
```

#### 2. Creating a new student:

URL: https://edu-x.herokuapp.com/students.json

**SUCCESSFUL REQUEST:**

EXAMPLE REQUEST:

```
curl -X POST \
  https://edu-x.herokuapp.com/students.json \
  -H 'Content-Type: application/json' \
  -d '{
	"student": {
		"name": "New Student name"
	}
}'
```

HTTP RESPONSE CODE: **200**

RESPONSE:

```json
{
  "name": "New Student name",
  "url": "https://edu-x.herokuapp.com/students/13.json"
}
```

**UNSUCCESSFUL REQUESTS:**

A. EXAMPLE REQUEST with incomplete parameters:

```
curl -X POST \
  https://edu-x.herokuapp.com/students.json \
  -H 'Content-Type: application/json' \
  -d '{
	"student": {
		"name": ""
	}
}'
```

HTTP RESPONSE CODE: **422**

RESPONSE:

```json
{ "errors": [{ "student": { "name": ["can't be blank"] } }] }
```

B. EXAMPLE REQUEST with no parameters:

```
curl -X POST \
  https://edu-x.herokuapp.com/students.json \
  -H 'Content-Type: application/json' \
  -d '{
}'
```

HTTP RESPONSE CODE: **422**

RESPONSE:

```json
{ "errors": [{ "student": ["parameter is required"] }] }
```

#### 3. Viewing student details, including progress of all the lessons they have completed:

URL: https://edu-x.herokuapp.com/students/{id}.json

EXAMPLE REQUEST:

```
curl -X GET \
  https://edu-x.herokuapp.com/students/1.json
```

HTTP RESPONSE CODE: **200**

RESPONSE:

```json
{
  "name": "Jed Kessler",
  "progress": [
    {
      "lesson_part": "Lesson: 1 Part: 2",
      "lesson_name": "Ameliorated",
      "part_name": "Robust disintermediate firmware",
      "course_name": "Bachelor of Nursing",
      "completed_at": "2018-07-21 22:38:46 UTC"
    },
    {
      "lesson_part": "Lesson: 1 Part: 1",
      "lesson_name": "Ameliorated",
      "part_name": "Reverse-engineered bi-directional conglomeration",
      "course_name": "Not taken with any course",
      "completed_at": "2018-07-21 22:38:37 UTC"
    }
  ]
}
```

#### 4. Creating a progress for a student:

URL: https://edu-x.herokuapp.com/students/{student_id}/progresses.json

**SUCCESSFUL REQUEST:**

A. EXAMPLE REQUEST without a course:

```
curl -X POST \
  https://edu-x.herokuapp.com/students/1/progresses.json \
  -H 'Content-Type: application/json' \
  -d '{
	"progress": {
		"lesson_number": 1,
		"part_number": 2
	}
}'
```

HTTP RESPONSE CODE: **201**

RESPONSE:

```json
{
  "lesson_part": "Lesson: 1 Part: 2",
  "lesson_name": "Ameliorated",
  "part_name": "Robust disintermediate firmware",
  "course_name": "Not taken with any course",
  "completed_at": "2018-07-22 00:37:51 UTC"
}
```

B. EXAMPLE REQUEST with a course:

```
curl -X POST \
  https://edu-x.herokuapp.com/students/1/progresses.json \
  -H 'Content-Type: application/json' \
  -d '{
	"progress": {
		"lesson_number": 1,
		"part_number": 2,
        "course_lesson_id": 1
	}
}'
```

HTTP RESPONSE CODE: **201**

RESPONSE:

```json
{
  "lesson_part": "Lesson: 1 Part: 2",
  "lesson_name": "Ameliorated",
  "part_name": "Robust disintermediate firmware",
  "course_name": "Bachelor of Nursing",
  "completed_at": "2018-07-22 00:40:09 UTC"
}
```

**UNSUCCESSFUL REQUESTS:**

A. EXAMPLE REQUEST with lesson and part where previous parts haven't been completed and where course provided doesn't offer the lesson:

```
curl -X POST \
  https://edu-x.herokuapp.com/students/1/progresses.json \
  -H 'Content-Type: application/json' \
  -d '{
        "progress": {
                "lesson_number": 5,
                "part_number": 2,
        "course_lesson_id": 1
        }
}'
```

HTTP RESPONSE CODE: **422**

RESPONSE:

```json
{
  "errors": [
    {
      "progress": {
        "part_id": [
          "Please complete 'Lesson: 5 Part: 1 | Lesson: Bi-directional | Part: Configurable reciprocal pricing structure' before trying to complete 'Lesson: 5 Part: 2 | Lesson: Bi-directional | Part: Multi-tiered impactful complexity'"
        ],
        "course_lesson_id": [
          "'Lesson: 5 Part: 2 | Lesson: Bi-directional | Part: Multi-tiered impactful complexity' is not offered with 'Course: Bachelor of Nursing | Lesson: 1 - Ameliorated'. Please select a course that offers 'Lesson: 5 - Bi-directional'"
        ]
      }
    }
  ]
}
```

B. EXAMPLE REQUEST with incomplete parameters:

```
curl -X POST \
  https://edu-x.herokuapp.com/students/1/progresses.json \
  -H 'Content-Type: application/json' \
  -d '{
        "progress": {
        }
}'
```

HTTP RESPONSE CODE: **422**

RESPONSE:

```json
{
  "errors": [
    { "progress": { "part": ["must exist"], "part_id": ["can't be blank"] } }
  ]
}
```

C. EXAMPLE REQUEST with no parameters:

```
curl -X POST \
  https://edu-x.herokuapp.com/students/1/progresses.json \
  -H 'Content-Type: application/json'
```

HTTP RESPONSE CODE: **422**

RESPONSE:

```json
{
  "errors": [
    { "progress": { "part": ["must exist"], "part_id": ["can't be blank"] } }
  ]
}
```

#### 5. Listing teachers:

URL: https://edu-x.herokuapp.com/teachers.json

EXAMPLE REQUEST:

```
curl -X GET \
  https://edu-x.herokuapp.com/teachers.json
```

HTTP RESPONSE CODE: **200**

RESPONSE:

```json
[
  {
    "name": "Mrs. Aldo Schmidt",
    "url": "https://edu-x.herokuapp.com/teachers/1.json"
  },
  { "name": "Noe Bauch", "url": "https://edu-x.herokuapp.com/teachers/2.json" },
  {
    "name": "Jeannie Parker",
    "url": "https://edu-x.herokuapp.com/teachers/3.json"
  },
  {
    "name": "Mrs. Lizzette Davis",
    "url": "https://edu-x.herokuapp.com/teachers/4.json"
  },
  {
    "name": "Mrs. Amado Steuber",
    "url": "https://edu-x.herokuapp.com/teachers/5.json"
  }
]
```

#### 6. Viewing teacher details, including report of progress of students, on the courses offered by the teacher:

URL: https://edu-x.herokuapp.com/teachers/{id}.json

EXAMPLE REQUEST:

```
curl -X GET \
  https://edu-x.herokuapp.com/teachers/3.json
```

HTTP RESPONSE CODE: **200**

RESPONSE:

```json
{
  "name": "Jeannie Parker",
  "report": [
    {
      "student": {
        "name": "Jed Kessler",
        "url": "https://edu-x.herokuapp.com/students/1.json",
        "progress": [
          {
            "lesson_part": "Lesson: 1 Part: 2",
            "lesson_name": "Ameliorated",
            "part_name": "Robust disintermediate firmware",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-22 00:40:09 UTC"
          },
          {
            "lesson_part": "Lesson: 1 Part: 2",
            "lesson_name": "Ameliorated",
            "part_name": "Robust disintermediate firmware",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 22:38:46 UTC"
          }
        ]
      }
    },
    {
      "student": {
        "name": "Miss Jacquie Schumm",
        "url": "https://edu-x.herokuapp.com/students/2.json",
        "progress": [
          {
            "lesson_part": "Lesson: 2 Part: 1",
            "lesson_name": "Seamless",
            "part_name": "Multi-layered fault-tolerant attitude",
            "course_name": "Associate Degree in Health Science",
            "completed_at": "2018-07-21 23:04:22 UTC"
          },
          {
            "lesson_part": "Lesson: 1 Part: 3",
            "lesson_name": "Ameliorated",
            "part_name": "Cross-platform zero defect definition",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 23:03:25 UTC"
          },
          {
            "lesson_part": "Lesson: 1 Part: 2",
            "lesson_name": "Ameliorated",
            "part_name": "Robust disintermediate firmware",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 23:02:20 UTC"
          },
          {
            "lesson_part": "Lesson: 1 Part: 1",
            "lesson_name": "Ameliorated",
            "part_name": "Reverse-engineered bi-directional conglomeration",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 23:02:02 UTC"
          }
        ]
      }
    },
    {
      "student": {
        "name": "Roderick Labadie DVM",
        "url": "https://edu-x.herokuapp.com/students/5.json",
        "progress": [
          {
            "lesson_part": "Lesson: 2 Part: 3",
            "lesson_name": "Seamless",
            "part_name": "Multi-tiered attitude-oriented toolset",
            "course_name": "Associate Degree in Health Science",
            "completed_at": "2018-07-21 23:07:24 UTC"
          },
          {
            "lesson_part": "Lesson: 2 Part: 2",
            "lesson_name": "Seamless",
            "part_name": "Re-engineered 24/7 initiative",
            "course_name": "Associate Degree in Health Science",
            "completed_at": "2018-07-21 23:07:16 UTC"
          },
          {
            "lesson_part": "Lesson: 2 Part: 1",
            "lesson_name": "Seamless",
            "part_name": "Multi-layered fault-tolerant attitude",
            "course_name": "Associate Degree in Health Science",
            "completed_at": "2018-07-21 23:07:05 UTC"
          },
          {
            "lesson_part": "Lesson: 1 Part: 3",
            "lesson_name": "Ameliorated",
            "part_name": "Cross-platform zero defect definition",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 23:06:57 UTC"
          },
          {
            "lesson_part": "Lesson: 1 Part: 2",
            "lesson_name": "Ameliorated",
            "part_name": "Robust disintermediate firmware",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 23:06:44 UTC"
          },
          {
            "lesson_part": "Lesson: 1 Part: 1",
            "lesson_name": "Ameliorated",
            "part_name": "Reverse-engineered bi-directional conglomeration",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 23:06:32 UTC"
          }
        ]
      }
    },
    {
      "student": {
        "name": "rita",
        "url": "https://edu-x.herokuapp.com/students/11.json",
        "progress": [
          {
            "lesson_part": "Lesson: 1 Part: 1",
            "lesson_name": "Ameliorated",
            "part_name": "Reverse-engineered bi-directional conglomeration",
            "course_name": "Bachelor of Nursing",
            "completed_at": "2018-07-21 23:00:29 UTC"
          }
        ]
      }
    }
  ]
}
```
