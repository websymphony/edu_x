class CreateCourseLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :course_lessons do |t|
      t.references :course, foreign_key: true
      t.references :lesson, foreign_key: true

      t.timestamps
    end
  end
end
