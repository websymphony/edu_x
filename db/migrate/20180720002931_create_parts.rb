class CreateParts < ActiveRecord::Migration[5.2]
  def change
    create_table :parts do |t|
      t.references :lesson, foreign_key: true
      t.references :previous, index: true
      t.integer :number
      t.string :name

      t.timestamps
    end
    add_foreign_key :parts, :parts, column: :previous_id
  end
end
