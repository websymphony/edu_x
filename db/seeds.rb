# frozen_string_literal: true

FactoryBot.create_list(:teacher, 5)
FactoryBot.create_list(:student, 10)

15.times do
  FactoryBot.create(:course,
                    teacher: Teacher.order(Arel.sql('RANDOM()')).first)
end

(1..100).each do |lesson_number|
  lesson = FactoryBot.create(:lesson, number: lesson_number)

  (1..3).each do |part_number|
    FactoryBot.create(:part,
                      lesson: lesson,
                      number: part_number,
                      previous_id: Part.last&.id)
  end
end

(1..20).each do |lesson_number|
  lesson = Lesson.find_by(number: lesson_number)
  FactoryBot.create(:course_lesson,
                    course: Course.order(Arel.sql('RANDOM()')).first,
                    lesson: lesson)
end
