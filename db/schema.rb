# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_20_003841) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "course_lessons", force: :cascade do |t|
    t.bigint "course_id"
    t.bigint "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_course_lessons_on_course_id"
    t.index ["lesson_id"], name: "index_course_lessons_on_lesson_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.bigint "teacher_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["teacher_id"], name: "index_courses_on_teacher_id"
  end

  create_table "lessons", force: :cascade do |t|
    t.string "name"
    t.integer "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parts", force: :cascade do |t|
    t.bigint "lesson_id"
    t.bigint "previous_id"
    t.integer "number"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_parts_on_lesson_id"
    t.index ["previous_id"], name: "index_parts_on_previous_id"
  end

  create_table "progresses", force: :cascade do |t|
    t.bigint "student_id"
    t.bigint "part_id"
    t.bigint "course_lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_lesson_id"], name: "index_progresses_on_course_lesson_id"
    t.index ["part_id"], name: "index_progresses_on_part_id"
    t.index ["student_id"], name: "index_progresses_on_student_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teachers", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "course_lessons", "courses"
  add_foreign_key "course_lessons", "lessons"
  add_foreign_key "courses", "teachers"
  add_foreign_key "parts", "lessons"
  add_foreign_key "parts", "parts", column: "previous_id"
  add_foreign_key "progresses", "course_lessons"
  add_foreign_key "progresses", "parts"
  add_foreign_key "progresses", "students"
end
