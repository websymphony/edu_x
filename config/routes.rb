Rails.application.routes.draw do
  root 'home#index'
  resources :students, only: [:index, :show, :new, :create] do
    resources :progresses, only: [:new, :create]
  end
  resources :teachers, only: [:index, :show]
end
