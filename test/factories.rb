# frozen_string_literal: true

FactoryBot.define do
  factory :student do
    name { Faker::Name.name }
  end

  factory :teacher do
    name { Faker::Name.name }
  end

  factory :lesson do
    name { Faker::Company.buzzword.capitalize }
  end

  factory :part do
    name { Faker::Company.catch_phrase }
    lesson
  end

  factory :course do
    name { Faker::Educator.course }
    teacher
  end

  factory :course_lesson do
    course
    lesson
  end

  factory :progress do
    student
    course_lesson
    part
  end
end
