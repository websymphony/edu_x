# frozen_string_literal: true

require 'test_helper'

class ProgressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student = FactoryBot.create(:student)
    lesson = FactoryBot.create(:lesson, number: 1)
    (1..3).each do |part_number|
      FactoryBot.create(:part,
                        lesson: lesson,
                        number: part_number,
                        previous_id: Part.last&.id)
    end
    course = FactoryBot.create(:course)
    @correct_course_lesson = FactoryBot.create(:course_lesson,
                                               lesson: lesson,
                                               course: course)
    @incorrect_course_lesson = FactoryBot.create(:course_lesson)
  end

  test 'should get new' do
    get new_student_progress_url(@student)
    assert_response :success
  end

  test 'should create progress with correct part in html format' do
    assert_difference('Progress.count') do
      post student_progresses_url(@student), params: { progress: { part_id: Part.first.id } }
    end

    assert_redirected_to student_url(@student)
  end

  test 'should create progress with correct part in json format' do
    part = Part.first
    assert_difference('Progress.count') do
      post student_progresses_url(@student, format: :json), params: { progress: { part_id: part.id } }
    end

    progress = Progress.last
    json_response = JSON.parse(response.body)

    expected_response = { 'lesson_part' => part.to_s,
                          'lesson_name' => part.lesson.name,
                          'part_name' => part.name,
                          'course_name' => progress.course_name,
                          'completed_at' => progress.created_at.to_s }
    assert_equal json_response, expected_response
  end

  test 'should create progress with correct part_number and lesson_number in html format' do
    assert_difference('Progress.count') do
      post student_progresses_url(@student), params: { progress: { lesson_number: 1, part_number: 1 } }
    end

    assert_redirected_to student_url(@student)
  end

  test 'should create progress with correct part_number and lesson_number in json format' do
    assert_difference('Progress.count') do
      post student_progresses_url(@student, format: :json), params: { progress: { lesson_number: 1, part_number: 1 } }
    end

    part = Part.first
    progress = Progress.last
    json_response = JSON.parse(response.body)

    expected_response = { 'lesson_part' => part.to_s,
                          'lesson_name' => part.lesson.name,
                          'part_name' => part.name,
                          'course_name' => progress.course_name,
                          'completed_at' => progress.created_at.to_s }
    assert_equal json_response, expected_response
  end

  test 'should create progress with correct part and course_lesson in html format' do
    assert_difference('Progress.count') do
      post student_progresses_url(@student), params: { progress: { part_id: Part.first.id, course_lesson_id: @correct_course_lesson.id } }
    end

    assert_redirected_to student_url(@student)
  end

  test 'should create progress with correct lesson_number, part_number and course_lesson in json format' do
    assert_difference('Progress.count') do
      post student_progresses_url(@student, format: :json), params: { progress: { lesson_number: 1, part_number: 1, course_lesson_id: @correct_course_lesson.id } }
    end

    part = Part.first
    progress = Progress.last
    json_response = JSON.parse(response.body)

    expected_response = { 'lesson_part' => part.to_s,
                          'lesson_name' => part.lesson.name,
                          'part_name' => part.name,
                          'course_name' => progress.course_name,
                          'completed_at' => progress.created_at.to_s }
    assert_equal json_response, expected_response
  end

  test 'should not create progress with correct part and incorrect course_lesson in html format' do
    assert_no_difference('Progress.count') do
      post student_progresses_url(@student), params: { progress: { part_id: Part.first.id,
                                                                   course_lesson_id: @incorrect_course_lesson.id } }
    end

    assert_template :new
  end

  test 'should not create progress with correct part and incorrect course_lesson in json format' do
    assert_no_difference('Progress.count') do
      post student_progresses_url(@student, format: :json), params: { progress: { lesson_number: 1,
                                                                                  part_number: 1,
                                                                                  course_lesson_id: @incorrect_course_lesson.id } }
    end

    assert_response 422
  end

  test 'should handle exception smoothly when no param data is provided' do
    assert_no_difference('Progress.count') do
      post student_progresses_url(@student, format: :json), params: { progress: {} }
    end
    json_response = JSON.parse(response.body)
    assert_equal json_response, 'errors' => [{ 'progress' => ['parameter is required'] }]
  end
end
