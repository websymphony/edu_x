# frozen_string_literal: true

require 'test_helper'

class StudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student = FactoryBot.create(:student)
  end

  test 'should get index in html format' do
    get students_url
    assert_response :success
  end

  test 'should get index in json format' do
    get students_url(format: :json)
    json_response = JSON.parse(response.body)

    expected_response = [
      {
        'name' => @student.name,
        'url' => student_url(@student, format: :json)
      }
    ]
    assert_equal json_response, expected_response
  end

  test 'should get new' do
    get new_student_url
    assert_response :success
  end

  test 'should create student in html format' do
    assert_difference('Student.count') do
      post students_url, params: { student: { name: 'Test Student' } }
    end

    assert_redirected_to student_url(Student.last)
  end

  test 'should create student in json format' do
    assert_difference('Student.count') do
      post students_url(format: :json), params: { student: { name: 'Test student' } }
    end

    json_response = JSON.parse(response.body)
    expected_response = {
      'name' => 'Test student',
      'url' => student_url(Student.last, format: :json)
    }

    assert_equal json_response, expected_response
  end

  test 'should not create student when no name is provided in html format' do
    assert_no_difference('Student.count') do
      post students_url, params: { student: { name: '' } }
    end

    assert_template :new
  end

  test 'should not create student when no name is provided in json format' do
    assert_no_difference('Student.count') do
      post students_url(format: :json), params: { student: { name: '' } }
    end

    json_response = JSON.parse(response.body)
    expected_response = { 'errors' => [{ 'student' => { 'name' => ["can't be blank"] } }] }

    assert_equal json_response, expected_response
  end

  test 'should not create student when no params are provided in json format' do
    assert_no_difference('Student.count') do
      post students_url(format: :json)
    end

    json_response = JSON.parse(response.body)
    expected_response = { 'errors' => [{ 'student' => ['parameter is required'] }] }

    assert_equal json_response, expected_response
  end

  test 'should show student in html format' do
    get student_url(@student)
    assert_response :success
  end

  test 'should show student in json format' do
    lesson = FactoryBot.create(:lesson, number: 1)
    part = FactoryBot.create(:part,
                             lesson: lesson,
                             number: 1,
                             previous_id: nil)
    course = FactoryBot.create(:course)
    course_lesson = FactoryBot.create(:course_lesson,
                                      lesson: lesson,
                                      course: course)
    progress = FactoryBot.create(:progress,
                                 student: @student,
                                 part: part,
                                 course_lesson: course_lesson)

    get student_url(@student, format: :json)
    json_response = JSON.parse(response.body)

    expected_response = { 'name' => @student.name,
                          'progress' => [
                            { 'lesson_part' => part.to_s,
                              'lesson_name' => lesson.name,
                              'part_name' => part.name,
                              'course_name' => course.name,
                              'completed_at' => progress.created_at.to_s }
                          ] }
    assert_equal json_response, expected_response
  end
end
