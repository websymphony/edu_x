# frozen_string_literal: true

require 'test_helper'

class TeachersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @teacher = FactoryBot.create(:teacher)
  end

  test 'should get index in html format' do
    get teachers_url
    assert_response :success
  end

  test 'should get index in json format' do
    get teachers_url(format: :json)
    json_response = JSON.parse(response.body)

    expected_response = [
      {
        'name' => @teacher.name,
        'url' => teacher_url(@teacher, format: :json)
      }
    ]
    assert_equal json_response, expected_response
  end

  test 'should show teacher in html format' do
    get teacher_url(@teacher)
    assert_response :success
  end

  test 'should show teacher in json format' do
    lesson = FactoryBot.create(:lesson, number: 1)
    part = FactoryBot.create(:part,
                             lesson: lesson,
                             number: 1,
                             previous_id: nil)
    course = FactoryBot.create(:course, teacher: @teacher)
    course_lesson = FactoryBot.create(:course_lesson,
                                      lesson: lesson,
                                      course: course)
    student = FactoryBot.create(:student)
    _non_reported_progress = FactoryBot.create(:progress,
                                               student: student,
                                               part: part,
                                               course_lesson: nil)
    progress = FactoryBot.create(:progress,
                                 student: student,
                                 part: part,
                                 course_lesson: course_lesson)
    get teacher_url(@teacher, format: :json)
    json_response = JSON.parse(response.body)

    expected_response = { 'name' => @teacher.name,
                          'report' => [
                            { 'student' =>
                              { 'name' => student.name,
                                'url' => student_url(student, format: :json),
                                'progress' => [
                                  { 'lesson_part' => part.to_s,
                                    'lesson_name' => lesson.name,
                                    'part_name' => part.name,
                                    'course_name' => course.name,
                                    'completed_at' => progress.created_at.to_s }
                                ] } }
                          ] }

    assert_equal json_response, expected_response
  end
end
