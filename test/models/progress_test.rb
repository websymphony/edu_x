# frozen_string_literal: true

require 'test_helper'

class ProgressTest < ActiveSupport::TestCase
  def setup
    @student = FactoryBot.create(:student)
    lesson = FactoryBot.create(:lesson, number: 1)
    (1..3).each do |part_number|
      FactoryBot.create(:part,
                        lesson: lesson,
                        number: part_number,
                        previous_id: Part.last&.id)
    end
    course = FactoryBot.create(:course)
    @correct_course_lesson = FactoryBot.create(:course_lesson,
                                               lesson: lesson,
                                               course: course)
    @incorrect_course_lesson = FactoryBot.create(:course_lesson)
  end

  test 'student can progress to first part of first lesson' do
    progress = @student.progresses.new(part: Part.first)
    assert_equal(progress.save, true)
  end

  test 'student cannot progress to last part of first lesson without progressing to previous part first' do
    progress = @student.progresses.new(part: Part.last)
    assert_equal(progress.save, false)
  end

  test 'student can progress to last part of first lesson if previous part is completed' do
    (1..2).each do |part_number|
      FactoryBot.create(:progress,
                        student: @student,
                        part: Part.find_by(number: part_number),
                        course_lesson: nil)
    end
    progress = @student.progresses.new(part: Part.last,
                                       course_lesson_id: @correct_course_lesson)
    assert_equal(progress.save, true)
  end

  test 'saving with course lesson that doesnt have lesson related to part will not save' do
    progress = @student.progresses.new(part: Part.first,
                                       course_lesson: @incorrect_course_lesson)
    assert_equal(progress.save, false)
  end

  test 'saving with course lesson that does lesson related to part will save' do
    progress = @student.progresses.new(part: Part.first,
                                       course_lesson: @correct_course_lesson)
    assert_equal(progress.save, true)
  end
end
